# Voici la liste des classes (Il y a toujours des ameliorations possible)
## User
+ id [int, AI, not null]
+ date_create [datetime, not null]
+ email [varchar, 255, not null]
+ password [varchar, 255, not null]


## Product
+ id [int, AI, not null]
+ name [varchar, 255, not null]
+ description [text, not null]
+ price [decimal(10,2), not null]
+ category_id [int]
+ images [text]
+ date_create [datetime, not null, default now]
+ date_edit [datetime, not null, default now]


## Category
+ id [int, AI, not null]
+ name [varchar, 100, not null]
+ description [text]
+ date_create [datetime, not null, default now]
+ date_edit [datetime, not null, default now]


# Il faut noter qu'on pourrait encore apporter plusieurs ameliorations sur cet outil, je mets ci-apres quelques exemples :

- Gestion de securite (integration continue, test unitaire)
- Gestion de l'interface en premier lieu
- Gestion des urls
- Dans le tableau de bord :
	* Mettre en place un apercu sur le nombre de ventes realisees
	* Un statistique sur le nombre les produits les plus vendus 
	* Mettre en place un apercu sur le nombre des produits enregistres avec les statuts actif ou non
	* Mettre en place un apercu sur le nombre de categorie
- Dans la gestion des produits :
	* Gerer les produits actifs/inactifs
	* Permettre d'ajouter plusieurs images pour un produits
	* Gerer le nombre de stock disponible pour les produits