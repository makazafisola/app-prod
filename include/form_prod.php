<?php
if (!empty($_GET['id']) && !empty($_GET['action']) && $_GET['action'] == 'edit') {
    $product = new Product();
    $prod = $product->getProduct(intval($_GET['id']));
}
?>

<div class="container-fluid p-5 bg-primary text-white text-center">
    <h1><?= $labelAction; ?> - Produit</h1>
</div>

<div class="container">
    <div class="vh-100 d-flex justify-content-center" style="margin-top: -20px;">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-12 col-md-8 col-lg-6">
                    <div class="card bg-white">
                        <div class="card-body p-5">
                            <form class="mb-3 mt-md-4" method="POST" action="product.php" enctype="multipart/form-data">
                                <input type="hidden" name="type-action" value="<?= $typeAction; ?>">
                                <input type="hidden" name="id" value="<?= !empty($prod['id']) ? $prod['id'] : ''; ?>">
                                <div class="mb-3">
                                    <label for="name" class="form-label ">Nom</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Nom du produit" value="<?= !empty($prod['id']) ? $prod['name'] : ''; ?>">
                                </div>
                                <div class="mb-3">
                                    <label for="description" class="form-label ">Description</label>
                                    <textarea class="form-control" name="description" id="description" placeholder="Entrer la description du produit"><?= !empty($prod['id']) ? $prod['description'] : ''; ?></textarea>
                                </div>
                                <div class="mb-3">
                                    <label for="price" class="form-label ">Prix</label>
                                    <input type="text" class="form-control" name="price" id="price" placeholder="10.5" value="<?= !empty($prod['id']) ? $prod['price'] : ''; ?>">
                                </div>
                                <div class="input-group mb-3">
                                    <select name="categ" class="form-select">
                                        <option value="0">Choisir la categorie</option>
                                        <?php
                                        $categorie = new Category();
                                        $categs = $categorie->getCategory();

                                        if (is_array($categs)) {
                                            foreach ($categs as $key => $categ) {
                                        ?>
                                                <option <?= (!empty($prod['category_id']) && $prod['category_id'] == $categ['id']) ? 'selected' : ''; ?> value="<?= $categ['id']; ?>"><?= $categ['name']; ?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="input-group mb-3">
                                    <input type="file" class="form-control" placeholder="Choisir une image du produit" name="images" aria-label="Choisir une image du produit" aria-describedby="basic-addon2">
                                </div>
                                <div class="d-grid">
                                    <button class="btn btn-outline-dark" type="submit">Enregister</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>