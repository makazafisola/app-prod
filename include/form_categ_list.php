<a class="btn btn-link" href="category.php?action=new">Ajouter nouvelle categorie</a>
<br>
<hr>
<br>
<p>Liste des categories</p>
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom de la categorie</th>
            <th scope="col">Description de la categorie</th>
            <th scope="col">Nb produits</th>
            <th scope="col">Creation</th>
            <th scope="col">Modification</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $categorie = new Category();
        $categs = $categorie->getCategory();

        $produit = new Product();

        if (is_array($categs)) {
            foreach ($categs as $key => $categ) {

                $produits = $produit->getProductByCateg($categ['id']);
        ?>
                <tr>
                    <td><?= $key + 1; ?></td>
                    <td><?= $categ['name']; ?></td>
                    <td><?= $categ['description']; ?></td>
                    <td>
                        <?php
                        if (count($produits) > 0) {
                        ?>
                            <a href="product.php?categ=<?= $categ['id']; ?>"><?= count($produits); ?></a>
                        <?php
                        } else {
                            echo "0";
                        }
                        ?>
                    </td>
                    <td><?= $categ['date_create']; ?></td>
                    <td><?= $categ['date_edit']; ?></td>
                    <td>
                        <a class="btn btn-xs btn-link" href="category.php?action=edit&id=<?= $categ['id']; ?>">Modifer</a>
                        <a class="btn btn-xs btn-danger" href="category.php?action=delete&id=<?= $categ['id']; ?>">Supprimer</a>
                    </td>
                </tr>
            <?php
            }
        } else {
            ?>

            <tr>
                <td class="text-center" colspan="6">Aucune categorie enregistree.</td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>