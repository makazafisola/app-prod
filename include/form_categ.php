<?php
if (!empty($_GET['id']) && !empty($_GET['action']) && $_GET['action'] == 'edit') {
    $category = new Category();
    $categ = $category->getCategory(intval($_GET['id']));
}
?>

<div class="container-fluid p-5 bg-primary text-white text-center">
    <h1><?= $labelAction; ?> - Categorie</h1>
</div>

<div class="container">
    <div class="vh-100 d-flex justify-content-center" style="margin-top: -20px;">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-12 col-md-8 col-lg-6">
                    <div class="card bg-white">
                        <div class="card-body p-5">
                            <form class="mb-3 mt-md-4" method="POST" action="category.php">
                                <input type="hidden" name="type-action" value="<?= $typeAction; ?>">
                                <input type="hidden" name="id" value="<?= !empty($categ['id']) ? $categ['id'] : ''; ?>">
                                <div class="mb-3">
                                    <label for="name" class="form-label ">Nom categorie</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Nom de la categorie" value="<?= !empty($categ['id']) ? $categ['name'] : ''; ?>">
                                </div>
                                <div class="mb-3">
                                    <label for="description" class="form-label ">Description categorie</label>
                                    <textarea class="form-control" name="description" id="description" placeholder="Entrer la description de la categorie"><?= !empty($categ['id']) ? $categ['description'] : ''; ?></textarea>
                                </div>
                                <div class="d-grid">
                                    <button class="btn btn-outline-dark" type="submit">Enregister</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>