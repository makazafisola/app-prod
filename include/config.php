<?php
session_start();

spl_autoload_register(function ($class) {
    require 'class/' . $class . '.php';
});

$_SERVER['HTTP_HOST'] = $_SERVER['HTTP_HOST'] . '/app-prod';

$PDO = new Database('localhost', 'db_appprod', 'root', '');
