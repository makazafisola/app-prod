<a class="btn btn-link" href="product.php?action=new">Ajouter nouveau produit</a>
<br>
<hr>
<br>
<p>Liste des produits</p>
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom du produit</th>
            <th scope="col">Description du produit</th>
            <th scope="col">Prix</th>
            <th scope="col">Categorie</th>
            <th scope="col">Images</th>
            <th scope="col">Creation</th>
            <th scope="col">Modification</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $produit = new Product();
        if (!empty($_GET['categ'])) {
            $list = $produit->getProductByCateg(intval($_GET['categ']));
        } elseif (!empty($_GET['filter'])) {
            $list = $produit->getProductByName($_GET['filter']);
        } else {
            $list = $produit->getProduct();
        }

        if (is_array($list) && count($list) > 0) {
            foreach ($list as $key => $prod) {
        ?>
                <tr>
                    <td><?= $key + 1; ?></td>
                    <td><?= $prod['name']; ?></td>
                    <td><?= $prod['description']; ?></td>
                    <td><?= $prod['price']; ?></td>
                    <td>
                        <?php
                        $categorie = new Category();
                        $categ = $categorie->getCategory($prod['category_id']);
                        if (!empty($categ['name'])) {
                            echo $categ['name'];
                        } else {
                            echo '<i>Non definie</i>';
                        }

                        ?>
                    </td>
                    <td>
                        <?php
                        if (!empty($prod['images'])) {
                        ?>
                            <img src="http://<?= $_SERVER['HTTP_HOST'] . '/' . $prod['images']; ?>" class="rounded mx-auto d-block" style="max-width: 200px;">
                        <?php
                        } else {
                            echo "<i>Aucun image</i>";
                        }
                        ?>
                    </td>
                    <td><?= $prod['date_create']; ?></td>
                    <td><?= $prod['date_edit']; ?></td>
                    <td>
                        <a class=" btn btn-xs btn-link" href="product.php?action=edit&id=<?= $prod['id']; ?>">Modifer</a>
                        <a class="btn btn-xs btn-danger" href="product.php?action=delete&id=<?= $prod['id']; ?>">Supprimer</a>
                    </td>
                </tr>
            <?php
            }
        } else {
            ?>

            <tr>
                <td class="text-center" colspan="9">Aucun produit enregistre.</td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>