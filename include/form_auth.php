<div class="container-fluid p-5 bg-primary text-white text-center">
    <h1>Authentification</h1>
</div>

<div class="container">
    <div class="vh-100 d-flex justify-content-center" style="margin-top: -20px;">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-12 col-md-8 col-lg-6">
                    <div class="card bg-white">
                        <div class="card-body p-5">
                            <form class="mb-3 mt-md-4" method="POST" action="login.php">
                                <?php
                                if (!empty($errorMsg)) {
                                ?>
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <strong>Erreur!</strong> <?= $errorMsg; ?>
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                <?php
                                }
                                ?>
                                <div class="mb-3">
                                    <label for="email" class="form-label ">Adresse Email</label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="email@email.com">
                                </div>
                                <div class="mb-3">
                                    <label for="password" class="form-label ">Mot de passe</label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="*******">
                                </div>
                                <div class="d-grid">
                                    <button class="btn btn-outline-dark" type="submit">Se connecter</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>