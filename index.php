<?php
require_once('include/config.php');

$user = new User();

if (!$user->checkSession()) {
    require_once('view/login.view.php');
} else {
    require_once('view/dashboard.view.php');
}
