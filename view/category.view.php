<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>
    <div class="container">
        <?php
        require_once('view/header.view.php');

        $typeAction = $option;
        switch ($option) {
            case 'list':
                require_once('include/form_categ_list.php');
                break;

            case 'edit':
                $labelAction = 'Modification';
                require_once('include/form_categ.php');
                break;

            case 'new':
                $labelAction = 'Creation';
                require_once('include/form_categ.php');
                break;
        }
        ?>
    </div>
</body>

</html>