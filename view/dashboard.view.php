<?php
require_once('include/config.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Outils de gestion de produit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>
    <div class="container">
        <?php
        require_once('view/header.view.php');
        ?>
        <div class="text-center">
            <div class="d-flex align-items-center mt-3 vh-100 flex-column">
                <form class="d-flex" method="GET" action="product.php">
                    <input class="form-control me-1" type="search" name="filter" placeholder="Saisir le nom du produit" aria-label="Rechercher">
                    <button class="btn btn-outline-primary" type="submit">Rechercher</button>
                </form>
            </div>
        </div>
    </div>
</body>

</html>