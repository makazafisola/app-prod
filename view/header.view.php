<div class="d-flex justify-content-center align-items-center">
    <a href="/app-prod/" class="col-sm-4 btn btn-block btn-info m-2">
        Tableau de bord
    </a>
    <a href="/app-prod/category.php" class="col-sm-4 btn btn-block btn-success m-2">
        Gestion des categories
    </a>
    <a href="/app-prod/product.php" class="col-sm-4 btn btn-block btn-primary m-2">
        Gestion des produits
    </a>
</div>