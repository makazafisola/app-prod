<?php

class Product
{
    private $id;
    private $name;
    private $description;
    private $price;
    private $category_id;
    private $images;
    private $date_create;
    private $date_edit;
    private $PDO;

    public function __construct()
    {
        $this->PDO = new Database;
    }

    public function createProduct(string $name, string $description, float $price, int $category_id, string $images): void
    {
        $sql = "INSERT INTO product (name, description, price, category_id, images, date_create, date_edit) 
                VALUES (:name, :description, :price, :category_id, :images, :date_create, :date_edit)";

        $params = [
            ':name' => $name,
            ':description' => $description,
            ':price' => $price,
            ':category_id' => $category_id,
            ':images' => $images,
            ':date_create' => date('Y-m-d H:i:s'),
            ':date_edit' => date('Y-m-d H:i:s')
        ];

        $stmt = $this->PDO->query($sql, $params, 'stmt');
        // var_dump($stmt);
        // $this->id = $stmt->lastInsertId();
    }

    public function getProduct(int $id = null): array
    {

        if (!empty($id)) {
            $sql = "SELECT * FROM product WHERE id = :id";
            $params = [
                ':id' => $id
            ];
        } else {
            $sql = "SELECT * FROM product";
            $params = [];
        }

        $product = $this->PDO->query($sql, $params, 'array');

        if ($product) {
            $product = (!empty($id)) ? $product[0] : $product;
            return $product;
        } else {
            return null;
        }
    }

    public function getProductByCateg(int $id): array
    {
        if (!empty($id)) {
            $sql = "SELECT * FROM product WHERE category_id = :id";
            $params = [
                ':id' => $id
            ];
        }

        $product = $this->PDO->query($sql, $params, 'array');

        if ($product) {
            return $product;
        } else {
            return [];
        }
    }

    public function getProductByName(string $filter): array
    {
        if (!empty($filter)) {
            $sql = "SELECT * FROM product WHERE name LIKE '%" . $filter . "%'";
            $params = [];
        }

        $product = $this->PDO->query($sql, $params, 'array');

        if ($product) {
            return $product;
        } else {
            return [];
        }
    }

    public function updateProduct(int $id, string $name, string $description, float $price, int $category_id, string $images): void
    {
        if (!$this->getProduct($id)) {
            throw new Exception("ID du produit non défini.");
        }

        $sql = "UPDATE product SET name = :name, description = :description, price = :price, category_id = :category_id, images = :images, date_edit = :date_edit WHERE id = :id";
        $params = [
            ':id' => $id,
            ':name' => $name,
            ':description' => $description,
            ':price' => $price,
            ':category_id' => $category_id,
            ':images' => $images,
            ':date_edit' => date('Y-m-d H:i:s')
        ];

        $result = $this->PDO->query($sql, $params, 'stmt');

        if ($result->rowCount() === 0) {
            throw new Exception("La mise à jour du produit a échoué.");
        }
    }

    public function deleteProduct(int $id): void
    {
        if (!$this->getProduct($id)) {
            throw new Exception("ID du produit non défini.");
        }

        $sql = "DELETE FROM product WHERE id = :id";
        $params = [
            ':id' => $id
        ];

        $result = $this->PDO->query($sql, $params, 'stmt');

        if ($result->rowCount() === 0) {
            throw new Exception("La suppression du produit a échoué.");
        }
    }
}
