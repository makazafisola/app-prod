<?php

class Category
{
    private $id;
    private $name;
    private $description;
    private $date_create;
    private $date_edit;
    private $PDO;

    public function __construct()
    {
        $this->PDO = new Database();
    }

    public function createCategory(string $name, string $description): void
    {
        $sql = "INSERT INTO category (name, description, date_create, date_edit) 
                VALUES (:name, :description, :date_create, :date_edit)";

        $params = [
            ':name' => $name,
            ':description' => $description,
            ':date_create' => date('Y-m-d H:i:s'),
            ':date_edit' => date('Y-m-d H:i:s')
        ];

        $stmt = $this->PDO->query($sql, $params, 'stmt');
        var_dump($stmt);
        // $this->id = $stmt->lastInsertId();
    }

    public function getCategory(int $id = null): ?array
    {

        if (!empty($id)) {
            $sql = "SELECT * FROM category WHERE id = :id";
            $params = [
                ':id' => $id
            ];
        } else {
            $sql = "SELECT * FROM category";
            $params = [];
        }

        $product = $this->PDO->query($sql, $params, 'array');

        if ($product) {
            $product = (!empty($id)) ? $product[0] : $product;
            return $product;
        } else {
            return null;
        }
    }

    public function updateCategory(int $id, string $name, string $description): void
    {
        if (!$this->getCategory($id)) {
            throw new Exception("ID de la categorie non défini.");
        }

        $sql = "UPDATE category SET name = :name, description = :description, date_edit = :date_edit WHERE id = :id";
        $params = [
            ':id' => $id,
            ':name' => $name,
            ':description' => $description,
            ':date_edit' => date('Y-m-d H:i:s')
        ];

        $result = $this->PDO->query($sql, $params, 'stmt');

        if ($result->rowCount() === 0) {
            throw new Exception("La mise à jour de la categorie a échoué.");
        }
    }

    public function deleteCategory(int $id): void
    {
        if (!$this->getCategory($id)) {
            throw new Exception("ID de la categorie non défini.");
        }

        $sql = "DELETE FROM category WHERE id = :id";
        $params = [
            ':id' => $id
        ];

        $result = $this->PDO->query($sql, $params, 'stmt');

        if ($result->rowCount() === 0) {
            throw new Exception("La suppression de la categorie a échoué.");
        }
    }
}
