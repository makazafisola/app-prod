<?php

class Database
{
    private $pdo;

    private $host = 'localhost';
    private $db = 'db_appprod';
    private $user = 'root';
    private $password = '';

    public function __construct()
    {
        if (empty($host)) {
            $host = $this->host;
        }

        if (empty($db)) {
            $db = $this->db;
        }

        if (empty($user)) {
            $user = $this->user;
        }

        if (empty($password)) {
            $pass = $this->password;
        }

        $dsn = "mysql:host=$host;dbname=$db";
        $this->pdo = new PDO($dsn, $user, $pass);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function query(string $sql, array $params = [], string $returnType = 'stmt')
    {
        $stmt = $this->pdo->prepare($sql);

        foreach ($params as $param => $value) {
            $type = PDO::PARAM_STR;
            if (is_int($value)) {
                $type = PDO::PARAM_INT;
            } elseif (is_bool($value)) {
                $type = PDO::PARAM_BOOL;
            } elseif (is_null($value)) {
                $type = PDO::PARAM_NULL;
            }

            // Lien du paramètre à la valeur
            $stmt->bindValue($param, $value, $type);
        }

        $stmt->execute();

        switch ($returnType) {
            case 'array':
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
                break;

            case 'stmt':
                return $stmt;
                break;
        }
    }
}
