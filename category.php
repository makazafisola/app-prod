<?php
require_once('include/config.php');

if (!empty($_POST['type-action'])) {
    switch ($_POST['type-action']) {
        case 'new':
            $category = new Category();

            $category->createCategory($_POST['name'], $_POST['description']);
            header("location: http://" . $_SERVER['HTTP_HOST'] . "/category.php");
            exit;
            break;
        case 'edit':
            $category = new Category();

            $category->updateCategory($_POST['id'], $_POST['name'], $_POST['description']);
            header("location: http://" . $_SERVER['HTTP_HOST'] . "/category.php");
            exit;
            break;
    }
} else {
    $_GET['action'] = !empty($_GET['action']) ? $_GET['action'] : 'list';

    switch ($_GET['action']) {
        case 'list':
            $title = 'Gestion des categories';
            break;

        case 'new':
            $title = 'Creation';
            break;

        case 'edit':
            $title = 'Modification';
            break;

        case 'delete':
            if (!empty($_GET['id'])) {
                $category = new Category();

                $category->deleteCategory($_GET['id']);
                header("location: http://" . $_SERVER['HTTP_HOST'] . "/category.php");
            }
            exit;
            break;

        default:
            $option = 'list';
            break;
    }

    $option = $_GET['action'];
    require_once('view/category.view.php');
}
