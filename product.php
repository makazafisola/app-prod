<?php
require_once('include/config.php');

if (!empty($_POST['type-action'])) {
    switch ($_POST['type-action']) {
        case 'new':
            $dest = "images/";
            $path = $dest . basename($_FILES["images"]["name"]);
            $down = false;

            if (move_uploaded_file($_FILES["images"]["tmp_name"], $path)) {
                $down = true;
            }

            $product = new Product();

            $product->createProduct($_POST['name'], $_POST['description'], $_POST['price'], $_POST['categ'], $path);
            header("location: http://" . $_SERVER['HTTP_HOST'] . '/product.php');
            exit;
            break;
        case 'edit':
            $dest = "images/";
            $path = $dest . basename($_FILES["images"]["name"]);
            $down = false;

            if (move_uploaded_file($_FILES["images"]["tmp_name"], $path)) {
                $down = true;
            }

            $product = new Product();

            $product->updateProduct($_POST['id'], $_POST['name'], $_POST['description'], $_POST['price'], $_POST['categ'], $path);
            header("location: http://" . $_SERVER['HTTP_HOST'] . '/product.php');
            exit;
            break;
    }
} else {
    $_GET['action'] = !empty($_GET['action']) ? $_GET['action'] : 'list';

    switch ($_GET['action']) {
        case 'list':
            $title = 'Gestion des produits';
            break;

        case 'new':
            $title = 'Creation';
            break;

        case 'edit':
            $title = 'Modification';
            break;

        case 'delete':
            if (!empty($_GET['id'])) {
                $product = new Product();

                $product->deleteProduct($_GET['id']);
                header("location: http://" . $_SERVER['HTTP_HOST']);
                exit;
            }
            break;

        default:
            $option = 'list';
            break;
    }

    $option = $_GET['action'];
    require_once('view/product.view.php');
}
