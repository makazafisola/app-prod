<?php
require_once "include/config.php";

if (!empty($_SESSION["userLogged"])) {
    header("location: http://" . $_SERVER['HTTP_HOST']);
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (empty(trim($_POST["email"]))) {
        $errorMsg = "Veuillez entrer votre email";
    } else {
        $email = trim($_POST["email"]);
    }

    if (empty(trim($_POST["password"]))) {
        $errorMsg = (empty($errorMsg)) ? "Veuillez entrer votre mot de passe" : " et votre mot de passe";
    } else {
        $password = trim($_POST["password"]);
    }

    if (empty($errorMsg)) {

        $user = new User();
        $user->setEmail($email);
        $user->setPassword($password);

        if ($user->checkUser()) {
            $_SESSION["userLogged"] = true;
            header("location: http://" . $_SERVER['HTTP_HOST']);
        } else {
            $title = 'Connexion - Utilisateur inconnu';
            $errorMsg = 'L\'utilisateur n\'est pas reconnu.';
            require_once('view/login.view.php');
        }
    } else {
        $title = 'Connexion - Utilisateur inconnu';
        require_once('view/login.view.php');
    }
} else {
    header("location: http://" . $_SERVER['HTTP_HOST']);
}
